{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Main where

import Control.Applicative ( empty )
import Data.Coerce ( coerce )
import Data.Foldable ( foldl' )
import Data.Int ( Int64 )
import Hedgehog ( MonadGen )
import ReingoldTilford.Algorithm ( render, petrify )
import ReingoldTilford.Types ( BinTree(..), XCoord, YCoord )
import Test.SmallCheck.Series ( Serial(..), Series, (\/), (<~>), getDepth, decDepth, localDepth )
import Test.Tasty ( TestTree, testGroup, localOption, defaultMain )

import qualified Data.Set as Set
import qualified Hedgehog as HH
import qualified Hedgehog.Gen as Gen
import qualified Test.Tasty.Hedgehog as HH
import qualified Test.Tasty.SmallCheck as SC

newtype SizedBinTree a = SizedBinTree (BinTree a)
  deriving newtype (Show)

-- Depth d controls # of nodes in the tree.
-- This instance is a helper to generate trees of __exactly__ size d,
-- which we can then union together to produce all trees of size __up to__ size d.
-- Does not produce duplicates.
--
-- The idea behind this construction is to notice that the set of all trees of
-- size n, K(n), can be partitioned into several smaller sets. When n >= 1,
-- K(n) = all nodes with left subtree in K(0) and right subtree in K(n-1)
--     \/ all nodes with left subtree in K(1) and right subtree in K(n-2)
--     \/ ...
--     \/ all nodes with left subtree in K(n-1) and right subtree in K(0)
-- Crucially, the intersection between any of these pairs of sets is the null
-- set, which means we don't ever have to do expensive duplicate testing.
instance Serial m a => Serial m (SizedBinTree a) where
  series =
    getDepth >>= \case
      0 -> pure $ SizedBinTree Leaf
      n -> foldl' (\gen size -> withSubSizes size (n-1-size) \/ gen) empty [0..n-1]
    where
      withSubSizes :: Int -> Int -> Series m (SizedBinTree a)
      withSubSizes ln rn =
        pure (\l r val -> SizedBinTree $ Node (coerce l) val (coerce r))
          <~> localDepth (const ln) (series :: Series m (SizedBinTree a))
          <~> localDepth (const rn) (series :: Series m (SizedBinTree a))
          <~> decDepth series

-- Depth d controls # of nodes in the tree.
-- Generates all trees up to size d.
instance Serial m a => Serial m (BinTree a) where
  series = do
    depth <- getDepth
    (\(SizedBinTree t) -> t) <$>
      foldl' (\gen size -> gen \/ localDepth (const size) series) empty [0..depth]

genBinTree :: MonadGen m => m a -> m (BinTree a)
genBinTree genVal =
  Gen.recursive Gen.choice
    [ pure Leaf ]
    [ Gen.subtermM2 (genBinTree genVal) (genBinTree genVal) $ \l r ->
      (\x -> Node l x r) <$> genVal
    ]

main :: IO ()
main = defaultMain $ withHedgehogConfig $ testGroup "reingold-tilford"
  [ testGroup "positioned nodes never overlap"
    [ SC.testProperty "for all trees up to size 8" $
        SC.changeDepth (const 8) $ SC.forAll noOverlap
    , HH.testProperty "for sampled trees" $
        HH.property $ do
          t <- HH.forAll (genBinTree $ Gen.constant ())
          HH.assert $ noOverlap t
    ]
  , testGroup "tree structure remains unchanged"
    [ SC.testProperty "for all trees up to size 8" $
        SC.changeDepth (const 8) $ SC.forAll sameTreeStructure
    , HH.testProperty "for sampled trees" $
        HH.property $ do
          t <- HH.forAll (genBinTree $ Gen.constant ())
          HH.assert $ sameTreeStructure t
    ]
  , testGroup "right children always to the right of left children"
    [ SC.testProperty "for all trees up to size 8" $
        SC.changeDepth (const 8) $ SC.forAll childrenPositionsAreOrdered
    , HH.testProperty "for sampled trees" $
        HH.property $ do
          t <- HH.forAll (genBinTree $ Gen.constant ())
          HH.assert $ childrenPositionsAreOrdered t
    ]
  , testGroup "rendered subtrees are rigid"
    [ SC.testProperty "for all trees up to size 8" $
        SC.changeDepth (const 8) $ SC.forAll rigidSubtrees
    , HH.testProperty "for sampled trees" $
        HH.property $ do
          t <- HH.forAll (genBinTree $ Gen.constant ())
          HH.assert $ rigidSubtrees t
    ]
  , testGroup "nodes on same level have same y coord"
    [ SC.testProperty "for all trees up to size 8" $
        SC.changeDepth (const 8) $ SC.forAll sameLevelSameYCoord
    , HH.testProperty "for sampled trees" $
        HH.property $ do
          t <- HH.forAll (genBinTree $ Gen.constant ())
          HH.assert $ sameLevelSameYCoord t
    ]
  ]
  where
    withHedgehogConfig :: TestTree -> TestTree
    withHedgehogConfig =
      localOption (HH.HedgehogShowReplay True)

noOverlap :: BinTree () -> Bool
noOverlap t =
  let
    rendered = petrify (0,0) $ render 2 t
    coords = toList rendered
  in length coords == Set.size (Set.fromList coords)
  where
    toList :: BinTree a -> [a]
    toList Leaf = []
    toList (Node l x r) = toList l ++ [x] ++ toList r

sameTreeStructure :: BinTree () -> Bool
sameTreeStructure t =
  let rendered = petrify (0,0) $ render 2 t
  in go t rendered
  where
    go :: BinTree () -> BinTree (XCoord, YCoord) -> Bool
    go Leaf Leaf = True
    go (Node l1 _ r1) (Node l2 _ r2) = go l1 l2 && go r1 r2
    go _ _ = False

childrenPositionsAreOrdered :: BinTree () -> Bool
childrenPositionsAreOrdered t =
  let rendered = petrify (0,0) $ render 2 t
  in go rendered
  where
    go :: BinTree (XCoord, YCoord) -> Bool
    go (Node l@(Node _ (lx, _) _) (x, _) r@(Node _ (rx, _) _)) =
      lx < x && x < rx && go l && go r
    go (Node l@(Node _ (lx, _) _) (x, _) Leaf) =
      lx < x && go l
    go (Node Leaf (x, _) r@(Node _ (rx, _) _)) =
      x < rx && go r
    go (Node Leaf _ Leaf) = True
    go Leaf = True

rigidSubtrees :: BinTree () -> Bool
rigidSubtrees Leaf = True
rigidSubtrees t@(Node l _ r) =
  case render 2 t of
    Leaf -> error "impossible"
    Node l' _ r' ->
      l' == render 2 l && r' == render 2 r

sameLevelSameYCoord :: BinTree () -> Bool
sameLevelSameYCoord t =
  let rendered = petrify (0,0) $ render 2 t
  in go rendered 0
  where
    go :: BinTree (XCoord, YCoord) -> Int64 -> Bool
    go Leaf _ = True
    go (Node l (_, y) r) lev =
      lev == y && go l (lev+1) && go r (lev+1)