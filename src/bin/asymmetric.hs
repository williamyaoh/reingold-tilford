module Main where

import Reanimate ( reanimate )
import ReingoldTilford.Animate ( animateTree )
import ReingoldTilford.Types ( example3 )

-- Animate the drawing of an asymmetric tree that also involves
-- following contours.
-- cabal v2-run render-asymmetric -- render -o <output>.mp4 --format mp4

main :: IO ()
main = reanimate $
  animateTree example3