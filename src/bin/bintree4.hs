module Main where

import Reanimate ( reanimate )
import ReingoldTilford.Animate ( animateTree )
import ReingoldTilford.Types ( completeBinTree )

-- Animate the drawing of a complete binary tree of height 4.
-- cabal v2-run render-bintree4 -- render -o <output>.mp4 --format mp4

main :: IO ()
main = reanimate $
  animateTree completeBinTree