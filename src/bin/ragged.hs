module Main where

import Reanimate ( reanimate )
import ReingoldTilford.Animate ( animateTree )
import ReingoldTilford.Types ( raggedContourTree )

-- Animate the drawing of a tree where we need to follow contours
-- rather than direct links in order to find the next push location.
-- cabal v2-run render-ragged -- render -o <output>.mp4 --format mp4

main :: IO ()
main = reanimate $
  animateTree raggedContourTree