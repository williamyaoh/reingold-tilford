{-# LANGUAGE DuplicateRecordFields        #-}
{-# LANGUAGE LambdaCase                   #-}
{-# LANGUAGE NamedFieldPuns               #-}
{-# LANGUAGE PatternSynonyms              #-}
{-# LANGUAGE ViewPatterns                 #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket"      #-}

-- |
-- Contains versions of the algorithm functions that also
-- emit events relevant to animating, and functions for
-- turning said events into animations of the
-- Reingold-Tilford algorithm.

module ReingoldTilford.Animate where

import Codec.Picture.Types ( PixelRGBA8(..) )
import Control.Lens ( (.~), (?~) )
import Control.Monad.Writer.Strict ( Writer )
import Data.Foldable ( Foldable(toList), foldl' )
import Data.Function ( (&) )
import Data.Maybe ( mapMaybe )
import Data.Sequence ( Seq )
import Data.Word ( Word64 )
import Graphics.SvgTree.Types
  ( pattern CircleTree, pattern LineTree, pattern PathTree, pattern GroupTree, pattern None
  , Number(..), Texture(..), PathCommand(..), Origin(..), Cap(..), LineJoin(..)
  , circleCenter, circleRadius
  , linePoint1, linePoint2
  , pathDefinition
  , groupChildren, groupOpacity
  , strokeWidth, strokeColor, strokeOpacity, strokeDashArray, strokeOffset, strokeLineCap, strokeLineJoin
  , fillColor, fillOpacity
  , defaultSvg,
  )
import Linear.V2 ( V2(..) )
import Reanimate
import Reanimate.Ease
import Reanimate.Builtin.Documentation
import ReingoldTilford.Types

import qualified Control.Monad.Writer.Strict as Writer
import qualified Data.Sequence as Seq
import qualified ReingoldTilford.Algorithm as Algo

data RenderEvent
  = InitTree
  | Descend !DescendParams
  | Push !PushParams
  | LayerDone !LayerDoneParams
  | FinalizeContourScan !FinalizeContourScanParams
  | FinalizeTree !FinalizeTreeParams
  deriving Show

data DescendParams = DescendParams
  { oldSep :: !Offset
  , newSep :: !Offset
  , oldLev :: !Word64
  , newLev :: !Word64
  , oldLeftPos :: !Offset
  , newLeftPos :: !Offset
  , rootDist :: !Offset
  }
  deriving Show

data PushParams = PushParams
  { oldSep :: !Offset
  , newSep :: !Offset
  , lev :: !Word64
  , -- | The position of the left node /before/ pushing.
    leftPos :: !Offset
  , -- | The root distance /before/ pushing.
    rootDist :: !Offset
  }
  deriving Show

data LayerDoneParams = LayerDoneParams
  { oldSep :: !Offset
  , newSep :: !Offset
  , lev :: !Word64
  , -- | The position of the left node /before/ pushing.
    leftPos :: !Offset
  , -- | The root distance /after/ pushing.
    rootDist :: !Offset
  }
  deriving Show

data FinalizeContourScanParams = FinalizeContourScanParams
  { sep :: !Offset
  , -- | The level *after* we found the last pair of nodes on the contours.
    lev :: !Word64
  , -- | The position of the left node /after/ pushing. Read that again!
    leftPos :: !Offset
  , rootDist :: !Offset
  }
  deriving Show

newtype FinalizeTreeParams = FinalizeTreeParams
  { childDist :: Distance }
  deriving Show

-- |
-- When initially returned from the contour scan, the left positions
-- in the render events will only be relative to the root of the
-- left subtree; we need to normalize their distance to the root
-- of the entire tree, based on what the current root separation
-- is. Similarly, the root distances need to be
-- halved.
normalizeEvents :: Seq RenderEvent -> Seq RenderEvent
normalizeEvents = fmap $ \case
  InitTree ->
    InitTree
  Descend params@DescendParams { rootDist } ->
    let dist = (rootDist + 1) `div` 2
    in Descend $ params
         { oldLeftPos = oldLeftPos params - dist
         , newLeftPos = newLeftPos params - dist
         , rootDist = dist
         }
  Push params@PushParams { leftPos = leftPos, rootDist } ->
    let dist = (rootDist + 1) `div` 2
    in Push $ params
         { leftPos = leftPos - dist
         , rootDist = dist
         }
  LayerDone params@LayerDoneParams { leftPos = leftPos, rootDist } ->
    let dist = (rootDist + 1) `div` 2
    in LayerDone $ params
         { leftPos = leftPos - dist
         , rootDist = dist
         }
  FinalizeContourScan params@FinalizeContourScanParams { leftPos = leftPos, rootDist } ->
    let dist = (rootDist + 1) `div` 2
    in FinalizeContourScan $ params
         { leftPos = leftPos - dist
         , rootDist = dist
         }
  FinalizeTree params ->
    FinalizeTree params

type WithEvents = Writer (Seq RenderEvent)

event :: RenderEvent -> WithEvents ()
event =
  Writer.tell . Seq.singleton

-- |
-- Assign child distances to each node in the tree, which can
-- be converted into (x, y) coordinates for each node by `petrify'.
render :: Distance -> BinTree a -> WithEvents (BinTree Distance)
render minsep t = do
  (t', _) <- go t
  pure $! t'
  where
    go :: BinTree a -> WithEvents (BinTree Distance, Contours)
    go = \case
      Leaf -> pure (Leaf, emptyContours)
      Node l _ r -> do
        (l', lctrs) <- go l
        (r', rctrs) <- go r
        scan <- scanContours minsep lctrs rctrs
        let rootDist = fromIntegral $ rootOffset scan
        ctrs <- spliceContours rootDist scan (lctrs, rctrs)
        pure (Node l' rootDist r', ctrs)

-- |
-- Given the contours for our two subtrees, construct the contours
-- for the whole tree, using the information we calculated from the contour scan.
spliceContours :: Distance -> ScanResult -> (Contours, Contours) -> WithEvents Contours
spliceContours (fromIntegral -> rootOffset) scan (Contours ll lr, Contours rl rr) =
  pure $! Contours leftContour rightContour
  where
    compareLength :: [a] -> [b] -> Ordering
    compareLength [] [] = EQ
    compareLength [] _y = LT
    compareLength _x [] = GT
    compareLength (_:xs) (_:ys) = compareLength xs ys

    leftContour :: Contour
    leftContour = case (rl `compareLength` ll, ll, rl) of
      (_, [], []) -> [0]
      (EQ, _, _) -> negate rootOffset : ll
      (LT, _, _) -> negate rootOffset : ll
      (GT, [], _) -> rootOffset : rl
      (GT, _, _) ->
        negate rootOffset : take (length ll - 1) ll ++ (rloffsum scan - lloffsum scan) : drop (length ll) rl

    rightContour :: Contour
    rightContour = case (lr `compareLength` rr, rr, lr) of
      (_, [], []) -> [0]
      (EQ, _, _) -> rootOffset : rr
      (LT, _, _) -> rootOffset : rr
      (GT, [], _) -> negate rootOffset : lr
      (GT, _, _) ->
        rootOffset : take (length rr - 1) rr ++ (lroffsum scan - rroffsum scan) : drop (length rr) lr
-- |
-- Recurse down the contours of our subtrees, calculating the separation
-- at the root.
scanContours :: Distance -> Contours -> Contours -> WithEvents ScanResult
scanContours (fromIntegral -> minsep) lctrs rctrs = do
  ScanResult rootSep lloffsum lroffsum rloffsum rroffsum <-
    go minsep 0 (ScanResult minsep 0 0 0 0) lctrs rctrs
  let rootOffset = (rootSep + 1) `div` 2

  event $ FinalizeTree $ FinalizeTreeParams
    { childDist = fromIntegral rootOffset }

  pure $! ScanResult
    rootOffset
    (lloffsum-rootOffset)
    (lroffsum-rootOffset)
    (rloffsum+rootOffset)
    (rroffsum+rootOffset)
  where
    go :: Offset -> Word64 -> ScanResult -> Contours -> Contours -> WithEvents ScanResult
    go cursep
       lev
       acc@(ScanResult rootSep lloffsum lroffsum rloffsum rroffsum)
       (Contours (x : ll) (y : lr))
       (Contours (z : rl) (w : rr)) = do
      -- cursep always = distance between y and z (or rather, the nodes they represent)
      (cursep', rootSep') <- if cursep < minsep
        then do
          let diff = minsep - cursep

          event $ Push $ PushParams
            { oldSep = cursep, newSep = minsep, lev = lev, leftPos = lroffsum, rootDist = rootSep }

          pure (minsep + z - y, rootSep + fromIntegral diff)
        else pure (cursep + z - y, rootSep)
      let (lloffsum', lroffsum', rloffsum', rroffsum') =
            (lloffsum+x, lroffsum+y, rloffsum+z, rroffsum+w)
          acc' = ScanResult rootSep' lloffsum' lroffsum' rloffsum' rroffsum'

      -- Note that due to the way the algorithm is implemented, the left positions in
      -- all events are relative to the root of the left subtree. A further normalization
      -- step to add in the root distance would be advisable.

      event $ LayerDone $ LayerDoneParams
        { oldSep = cursep, newSep = cursep `max` minsep, lev = lev, leftPos = lroffsum, rootDist = rootSep' }
      event $ Descend $ DescendParams
        { oldSep = cursep `max` minsep, newSep = cursep', oldLev = lev, newLev = lev+1, oldLeftPos = lroffsum, newLeftPos = lroffsum', rootDist = rootSep' }

      go cursep' (lev+1) acc' (Contours ll lr) (Contours rl rr)
    go cursep lev acc _ _ = do

      event $ FinalizeContourScan $
        FinalizeContourScanParams { sep = cursep, lev = lev, leftPos = lroffsum acc, rootDist = rootOffset acc }

      pure $! acc

{- REANIMATE CODE -}

interpolateColor :: PixelRGBA8 -> PixelRGBA8 -> Time -> PixelRGBA8
interpolateColor start end =
  case (start, end) of
    (PixelRGBA8 r1 g1 b1 a1, PixelRGBA8 r2 g2 b2 a2) ->
      let diffR = fi r2 - fi r1
          diffG = fi g2 - fi g1
          diffB = fi b2 - fi b1
          diffA = fi a2 - fi a1
      in \t -> PixelRGBA8
           (truncate $ fi r1 + diffR * t)
           (truncate $ fi g1 + diffG * t)
           (truncate $ fi b1 + diffB * t)
           (truncate $ fi a1 + diffA * t)
  where
    fi = fromIntegral

animationEvents :: Distance -> BinTree a -> Seq RenderEvent
animationEvents dist tree =
  case Writer.runWriter $ render dist tree of
    (Leaf, evs) -> evs
    (Node _ rootDist _, evs) -> normalizeEvents evs

-- |
-- We assume that all trees get rendered at (0, 0).
translateCoords :: (XCoord, YCoord) -> (Double, Double)
translateCoords (x, y) =
  (fromIntegral x * 0.9, negate (fromIntegral y) * 1.3 + 3.5)

nodeSVG :: (Double, Double) -> PixelRGBA8 -> SVG
nodeSVG (x, y) color =
  CircleTree $ defaultSvg
    & circleCenter .~ (Num x, Num y)
    & circleRadius .~ Num 0.35
    & strokeWidth ?~ Num 0.1
    & strokeColor ?~ ColorRef color
    & strokeOpacity ?~ 1.0
    & fillOpacity ?~ 0.0

treeSVG :: BinTree (XCoord, YCoord)  -> (Double, Double) -> PixelRGBA8 -> SVG
treeSVG t (rootX, rootY) color =
  spines t Nothing
  where
    spines :: BinTree (XCoord, YCoord) -> Maybe (Double, Double) -> SVG
    spines Leaf _ = None
    spines (Node l (translateCoords -> (x, y)) r) mparent =
      GroupTree $ defaultSvg
        & groupChildren .~
          reverse
            ( nodeSVG (x + rootX, y + rootY) color
            : spines l (Just (x, y))
            : spines r (Just (x, y))
            : case mparent of
                Nothing -> []
                Just (parentX, parentY) ->
                  let toParent = (parentX - x, parentY - y)
                      dist = distance toParent
                      unit = unitVector toParent
                  in [ LineTree $ defaultSvg
                         & linePoint1 .~ point (vecAdd (rootX, rootY) $ vecAdd (x, y) (vecMul unit 0.35))
                         & linePoint2 .~ point (vecAdd (rootX, rootY) $ vecAdd (parentX, parentY) (vecMul unit (-0.35)))
                         & strokeWidth ?~ Num 0.1
                         & strokeColor ?~ ColorRef color
                         & strokeOpacity ?~ 1.0
                     ]
            )


contourSVG :: Contour -> (Double, Double) -> SVG
contourSVG ctr (rootX, rootY) =
  contour ctr 1 0 Nothing
  where
    contour :: Contour -> Word64 -> Offset -> Maybe Offset -> SVG
    contour [] _ _ _ = None
    contour (here : rest) lev offset mparent =
      GroupTree $ defaultSvg
        & groupChildren .~
          [ case mparent of
              Nothing -> None
              Just parent ->
                let parentPos = translateCoords (parent, fromIntegral lev - 1) `vecAdd` (rootX, rootY)
                    herePos = translateCoords (offset, fromIntegral lev) `vecAdd` (rootX, rootY)
                    toParent = vecAdd parentPos (vecMul herePos (-1))
                    unit = unitVector toParent
                in LineTree $ defaultSvg
                     & linePoint1 .~ point (vecAdd parentPos (vecMul unit (-0.35)))
                     & linePoint2 .~ point (vecAdd herePos (vecMul unit 0.35))
                     & strokeWidth ?~ Num 0.1
                     & strokeColor ?~ ColorRef (PixelRGBA8 0x80 0x80 0x80 0xFF)
                     & strokeDashArray ?~ [Num 0.1, Num 0.1]
                     & strokeOpacity ?~ 1.0
                     & fillOpacity ?~ 0.0
          , contour rest (lev+1) (offset+here) (Just offset)
          ]

checkSVG :: (Double, Double) -> SVG
checkSVG (cx, cy) =
  GroupTree $ defaultSvg
    & groupChildren .~
      [ CircleTree $ defaultSvg
          & circleCenter .~ (Num cx, Num cy)
          & circleRadius .~ Num 0.35
          & strokeOpacity ?~ 0.0
          & fillColor ?~ ColorRef mantis
          & fillOpacity ?~ 1.0
      , PathTree $ defaultSvg
          & strokeOpacity ?~ 1.0
          & strokeColor ?~ ColorRef white
          & strokeWidth ?~ Num 0.08
          & strokeLineCap ?~ CapRound
          & strokeLineJoin ?~ JoinRound
          & fillOpacity ?~ 0.0
          & pathDefinition .~
            [ MoveTo OriginAbsolute [V2 (cx - 0.21) (cy - 0.0350)]
            , LineTo OriginRelative [V2 0.14 (-0.14)]
            , LineTo OriginRelative [V2 0.28 0.28]
            ]
      ]


sepSVG :: (Double, Double) -> (Double, Double) -> SVG
sepSVG (leftX, leftY) (rightX, rightY) =
  GroupTree $ defaultSvg
    & groupChildren .~
      [ if abs (rightX - leftX) < 0.7 then None
        else if leftX < rightX then
          LineTree $ defaultSvg
            & linePoint1 .~ (Num (leftX + 0.35), Num leftY)
            & linePoint2 .~ (Num (rightX - 0.35), Num rightY)
            & strokeWidth ?~ Num 0.15
            & strokeColor ?~ ColorRef color
            & strokeOpacity ?~ 1.0
        else
          LineTree $ defaultSvg
            & linePoint1 .~ (Num (leftX - 0.35), Num leftY)
            & linePoint2 .~ (Num (rightX + 0.35), Num rightY)
            & strokeWidth ?~ Num 0.15
            & strokeColor ?~ ColorRef color
            & strokeOpacity ?~ 1.0
      , CircleTree $ defaultSvg
          & circleCenter .~ (Num leftX, Num leftY)
          & circleRadius .~ Num 0.35
          & strokeWidth ?~ Num 0.15
          & strokeColor ?~ ColorRef color
          & strokeOpacity ?~ 1.0
          & strokeDashArray ?~ [Num (0.35 * 2 * pi * 0.25 * 0.75), Num (0.35 * 2 * pi * 0.25 * 0.25)]
          & strokeOffset ?~ Num (0.35 * pi * 0.25 * 0.75)
          & fillOpacity ?~ 0.0
      , CircleTree $ defaultSvg
          & circleCenter .~ (Num rightX, Num rightY)
          & circleRadius .~ Num 0.35
          & strokeWidth ?~ Num 0.15
          & strokeColor ?~ ColorRef color
          & strokeOpacity ?~ 1.0
          & strokeDashArray ?~ [Num (0.35 * 2 * pi * 0.25 * 0.75), Num (0.35 * 2 * pi * 0.25 * 0.25)]
          & strokeOffset ?~ Num (0.35 * pi * 0.25 * 0.75)
          & fillOpacity ?~ 0.0
      ]
  where
    lengthFactor :: Double
    lengthFactor = (((rightX - leftX - 0.9) / 0.9) `max` 0) `min` 1

    color :: PixelRGBA8
    color = interpolateColor red mantis lengthFactor

animateTree :: BinTree a -> Animation
animateTree t = docEnv $
  foldl'
    (\last next -> last `seqA` toAnim next)
    (toAnim InitTree)
    (normalizeEvents events)
  where
    Node l _ r = t
    -- We want to intentionally overlay the subtrees at the start.
    (Algo.petrify (0, 1) -> l', lctrs) = Algo.render' 2 l
    (Algo.petrify (0, 1) -> r', rctrs) = Algo.render' 2 r
    (rootDist, events) = Writer.runWriter $ do
      scan <- scanContours 2 lctrs rctrs
      let rootDist = fromIntegral (rootOffset scan)
      _ <- spliceContours rootDist scan (lctrs, rctrs)
      pure rootDist

    toAnim = \case
      InitTree -> initTree (l', lctrs, r', rctrs)
      Descend params -> descendLayer (l', lctrs, r', rctrs) params
      Push params -> pushLayer (l', lctrs, r', rctrs) params
      LayerDone params -> layerDone (l', lctrs, r', rctrs) params
      FinalizeContourScan params -> finalizeContourScan (l', lctrs, r', rctrs) params
      FinalizeTree params -> finalizeTree (l', lctrs, r', rctrs) params

type TreeInfo =
  ( BinTree (XCoord, YCoord)
  , Contours
  , BinTree (XCoord, YCoord)
  , Contours
  )

initTree :: TreeInfo -> Animation
initTree (l, lctrs, r, rctrs) =
  signalA (curveS 2) (mkAnimation 0.5 (\t ->
    GroupTree $ defaultSvg
      & groupChildren .~
        [ nodeSVG (translateCoords (0, 0)) black
        , treeSVG l (0, 0) rust
        , contourSVG (right lctrs) (0, 0)
        , treeSVG r (0, 0) lapisLazuli
        , contourSVG (left rctrs) (0, 0)
        , sepSVG (translateCoords (0, 1)) (translateCoords (0, 1))
        ]
      & groupOpacity ?~ fromRational (toRational t)))
    `andThen` pause 0.2
    `seqA` pushLayer (l, lctrs, r, rctrs) (PushParams 0 2 0 0 0)

descendLayer :: TreeInfo -> DescendParams -> Animation
descendLayer (l, lctrs, r, rctrs) (DescendParams { oldSep, newSep, oldLev, newLev, oldLeftPos, newLeftPos, rootDist }) =
  signalA (curveS 2) (mkAnimation 0.4 $ \t ->
    GroupTree $ defaultSvg
      & groupChildren .~
        [ nodeSVG (translateCoords (0, 0)) black
        , treeSVG l (fromIntegral rootDist * (-0.9),0) rust
        , contourSVG (right lctrs) (fromIntegral rootDist * (-0.9),0)
        , treeSVG r (fromIntegral rootDist * 0.9,0) lapisLazuli
        , contourSVG (left rctrs) (fromIntegral rootDist * 0.9,0)
        , let (startLeftX, startLeftY) = translateCoords (oldLeftPos, fromIntegral oldLev + 1)
              (startRightX, startRightY) = translateCoords (oldLeftPos + oldSep, fromIntegral oldLev + 1)
              (endLeftX, endLeftY) = translateCoords (newLeftPos, fromIntegral newLev + 1)
              (endRightX, endRightY) = translateCoords (newLeftPos + newSep, fromIntegral newLev + 1)
          in sepSVG (startLeftX + (endLeftX - startLeftX) * t, startLeftY + (endLeftY - startLeftY) * t)
                    (startRightX + (endRightX - startRightX) * t, startRightY + (endRightY - startRightY) * t)
        ])
    `andThen` pause 0.2

pushLayer :: TreeInfo -> PushParams -> Animation
pushLayer (l, lctrs, r, rctrs) (PushParams { oldSep, newSep, lev, leftPos, rootDist }) =
  signalA (curveS 2) $ mkAnimation 0.4 $ \t ->
    let (startLeftX, startLeftY) = translateCoords (leftPos, fromIntegral lev+1)
        (startRightX, startRightY) = translateCoords (leftPos + oldSep, fromIntegral lev+1)
        diffSep = (fromIntegral newSep - fromIntegral oldSep) * t * 0.9
        (endLeftX, endLeftY) = (startLeftX - diffSep / 2, startLeftY)
        (endRightX, endRightY) = (startRightX + diffSep / 2, startRightY)
    in GroupTree $ defaultSvg
         & groupChildren .~
           [ nodeSVG (translateCoords (0, 0)) black
           , treeSVG l (negate (diffSep/2) + (fromIntegral rootDist * (-0.9)), 0) rust
           , contourSVG (right lctrs) (negate (diffSep/2) + (fromIntegral rootDist * (-0.9)), 0)
           , treeSVG r (diffSep/2 + (fromIntegral rootDist * 0.9), 0) lapisLazuli
           , contourSVG (left rctrs) (diffSep/2 + (fromIntegral rootDist * 0.9), 0)
           , sepSVG (endLeftX, endLeftY) (endRightX, endRightY)
           ]

layerDone :: TreeInfo -> LayerDoneParams -> Animation
layerDone (l, lctrs, r, rctrs) (LayerDoneParams { oldSep, newSep, lev, leftPos, rootDist }) =
  staticFrame 0.2 (g withoutMark)
    `seqA` staticFrame 0.1 (g withMark)
    `seqA` staticFrame 0.1 (g withoutMark)
    `seqA` staticFrame 0.5 (g withMark)
  where
    (leftX, leftY) = translateCoords (leftPos, fromIntegral lev+1)
    (rightX, rightY) = translateCoords (leftPos + newSep, fromIntegral lev+1)

    withoutMark :: [SVG]
    withoutMark =
      [ nodeSVG (translateCoords (0, 0)) black
      , treeSVG l (fromIntegral rootDist * (-0.9), 0) rust
      , contourSVG (right lctrs) (fromIntegral rootDist * (-0.9), 0)
      , treeSVG r (fromIntegral rootDist * 0.9, 0) lapisLazuli
      , contourSVG (left rctrs) (fromIntegral rootDist * 0.9, 0)
      , sepSVG (leftX, leftY) (rightX, rightY)
      ]

    withMark :: [SVG]
    withMark = withoutMark ++ [checkSVG ((leftX + rightX) / 2, leftY)]

    g :: [SVG] -> SVG
    g children = GroupTree $ defaultSvg
      & groupChildren .~ children


finalizeContourScan :: TreeInfo -> FinalizeContourScanParams -> Animation
finalizeContourScan (l, lctrs, r, rctrs) (FinalizeContourScanParams { sep, lev, leftPos, rootDist }) =
  signalA (curveS 2) $ mkAnimation 1.0 $ \t ->
    let (leftX, leftY) = translateCoords (leftPos, fromIntegral lev+1)
        (rightX, rightY) = translateCoords (leftPos + sep, fromIntegral lev+1)
    in GroupTree $ defaultSvg
         & groupChildren .~
           [ nodeSVG (translateCoords (0, 0)) black
           , treeSVG l (fromIntegral rootDist * (-0.9), 0) (interpolateColor rust black t)
           , treeSVG r (fromIntegral rootDist * 0.9, 0) (interpolateColor lapisLazuli black t)
           , GroupTree $ defaultSvg
               & groupChildren .~
                 [ contourSVG (right lctrs) (fromIntegral rootDist * (-0.9), 0)
                 , contourSVG (left rctrs) (fromIntegral rootDist * 0.9, 0)
                 , sepSVG (leftX, leftY) (rightX, rightY)
                 ]
               & groupOpacity ?~ fromRational (toRational (1-t))
           ]

finalizeTree :: TreeInfo -> FinalizeTreeParams -> Animation
finalizeTree (l, lctrs, r, rctrs) (FinalizeTreeParams { childDist }) =
  signalA (curveS 2) (mkAnimation 1.0 $ \t ->
    let rootPos@(rootX, rootY) = translateCoords (0, 0)
        (leftX, leftY) = translateCoords (negate $ fromIntegral childDist, 1)
        (rightX, rightY) = translateCoords (fromIntegral childDist, 1)
        leftVec = (leftX - rootX, leftY - rootY)
        rightVec = (rightX - rootX, rightY - rootY)
        leftDist = distance leftVec
        rightDist = distance rightVec
        towardsLeft = unitVector leftVec
        towardsRight = unitVector rightVec
    in GroupTree $ defaultSvg
         & groupChildren .~
           [ nodeSVG (translateCoords (0, 0)) black
           , treeSVG l (fromIntegral childDist * (-0.9), 0) black
           , treeSVG r (fromIntegral childDist * 0.9, 0) black
           , LineTree $ defaultSvg
               & linePoint1 .~ point (rootPos `vecAdd` vecMul towardsLeft 0.35)
               & linePoint2 .~ point (rootPos `vecAdd` vecMul towardsLeft 0.35 `vecAdd` vecMul towardsLeft ((leftDist - 0.7) * t))
               & strokeWidth ?~ Num 0.1
               & strokeColor ?~ ColorRef black
               & strokeOpacity ?~ 1.0
           , LineTree $ defaultSvg
               & linePoint1 .~ point (rootPos `vecAdd` vecMul towardsRight 0.35)
               & linePoint2 .~ point (rootPos `vecAdd` vecMul towardsRight 0.35 `vecAdd` vecMul towardsRight ((rightDist - 0.7) * t))
               & strokeWidth ?~ Num 0.1
               & strokeColor ?~ ColorRef black
               & strokeOpacity ?~ 1.0
           ])
    `andThen` pause 1.0

point :: (Double, Double) -> (Number, Number)
point (x, y) = (Num x, Num y)

distance :: (Double, Double) -> Double
distance (x, y) = sqrt $ (x^2) + (y^2)

unitVector :: (Double, Double) -> (Double, Double)
unitVector (x, y) =
  let z = distance (x, y)
  in (x / z, y / z)

vecAdd :: (Double, Double) -> (Double, Double) -> (Double, Double)
vecAdd (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

vecMul :: (Double, Double) -> Double -> (Double, Double)
vecMul (x, y) n = (x*n, y*n)

black :: PixelRGBA8
black = PixelRGBA8 0x00 0x00 0x00 0xFF

white :: PixelRGBA8
white = PixelRGBA8 0xFF 0xFF 0XFF 0xFF

lapisLazuli :: PixelRGBA8
lapisLazuli = PixelRGBA8 0x23 0x57 0x89 0xFF

rust :: PixelRGBA8
rust = PixelRGBA8 0xAE 0x43 0x0A 0xFF

red :: PixelRGBA8
red = PixelRGBA8 0xFF 0x33 0x33 0xFF

mantis :: PixelRGBA8
mantis = PixelRGBA8 0x59 0xC5 0x5E 0xFF

eminence :: PixelRGBA8
eminence = PixelRGBA8 0x7F 0x5C 0xAD 0xFF