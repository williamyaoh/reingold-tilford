# reingold-tilford

A reimplementation of the paper [Tidier Drawings of Trees, 1981](https://reingold.co/tidier-drawings.pdf) in Haskell. Also includes a module for producing
animations of trees being drawn using the algorithm.

Property tests are included that test against all trees up to 8 nodes, randomly
sampling trees above that.
